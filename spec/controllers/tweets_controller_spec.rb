require 'rails_helper'

describe TweetsController, type: :controller do
  describe '#index' do
    context 'GET' do
      it 'return a success response' do
        get :index
        expect(response.status).to eq(200)
      end
    end
  end
end
